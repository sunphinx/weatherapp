var app = angular.module("weatherApp", []);

app.controller("WeatherController", function($scope, $http) {
  $scope.search = "";
  $scope.error = "";
  var reg = /[a-zA-Z\s]+/;
  $scope.savedTemperatures = []
    //check if  enter button pressed inside search field
  $scope.getResultEnterbutton = function(event) {
    if (event.charCode == 13)
      $scope.results();
  }

  //get results from openweathermap api using data inserted to search field
  $scope.results = function(search) {
    $scope.success = "";
    $scope.error = ""
    $scope.name = "";
    $scope.temperature = "";

    //enter your API key here
    //for example: $scope.yourApiKey = "s291asdfd832easda2a9f23194458587";
    $scope.yourApiKey = "ENTERAPIKEY";

    if (reg.test($scope.search) == true && $scope.search != undefined) {
      //get api data, where $scope.search = city entered
      $scope.http = "http://api.openweathermap.org/data/2.5/weather?q=" + $scope.search + "&APPID=" + $scope.yourApiKey + "&units=Metric";
      $http.get($scope.http)
        .success(function(data, response) {
          $scope.name = data.name + ": ";
          $scope.temperature = data.main.temp + " C";
          $scope.search = "";
        })
        .error(function(data) {
          $scope.error = "No data found";
        });
    } else {
      $scope.error = "Insert city name, alphanumeric only (A-Z)";
    }

    $scope.save = function() {
      $scope.success = "";
      $scope.items = $scope.name + $scope.temperature;
      if ($scope.items != "") {
        //check if $scope.items already exists in array of $scope.savedTemperatures
        if ($scope.savedTemperatures.indexOf($scope.items) == -1) {
          $scope.savedTemperatures.push($scope.items);
          $scope.name = "";
          $scope.temperature = "";
          $scope.success = "Data inserted successfully";
        } else {
          $scope.error = "Data already exists";
        }
      }
    }
  }
})
