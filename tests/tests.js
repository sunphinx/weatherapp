describe('WeatherController', function() {

  beforeEach(module('weatherApp'));

  var scope;
  beforeEach(inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    event = 13;

    $controller("WeatherController", {
      $scope: scope
    });
  }));

  it("$scope.search should be empty at first", function() {
    scope.search;
    expect(scope.search).toBe("");
  });

  it("getResultEnterButton -function should not run as event has not passed the charCode to it", function() {
    spyOn(scope, 'getResultEnterbutton');
    spyOn(scope, 'results');
    expect(scope.results).not.toHaveBeenCalled();
  });
});
