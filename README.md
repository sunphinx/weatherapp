WeatherApp - Version 1

Description
-Search for cities and get their weather temperatures with the use of OpenWeatherMap API. You can also save the result on the browser.

Get Up and Running
-To get the app running, you must install AngularJS, Karma, Jasmine, ngMock and Browsers(for an example PhantomJS).
-In js/app.js you should enter your valid OpenWeatherMap API key on line 23 as a string. You can get your key by signing up to their website at http://openweathermap.org/.